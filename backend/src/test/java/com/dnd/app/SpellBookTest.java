package com.dnd.app;

import com.dnd.app.dto.character.CharacterDTO;
import com.dnd.app.dto.character.SpellBookDTO;
import com.dnd.app.dto.security.AuthResponseDTO;
import com.dnd.app.dto.security.LoginRequestDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:test.properties")
class SpellBookTest {

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    private String characterData;
    private String spellBookData;
    private String webToken;
    private String loginData;

    @BeforeEach
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
        File testData = new File("src/test/resources/json/character.json");
        characterData = objectMapper.writeValueAsString(objectMapper.readValue(testData, CharacterDTO.class));
        testData = new File("src/test/resources/json/auth.json");
        loginData = objectMapper.writeValueAsString(objectMapper.readValue(testData, LoginRequestDTO.class));
        testData = new File("src/test/resources/json/spellbook.json");
        spellBookData = objectMapper.writeValueAsString(objectMapper.readValue(testData, SpellBookDTO.class));

        MvcResult result = mockMvc.perform(post("/user/singin")
                .content(loginData)
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        AuthResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDTO.class);
        webToken = response.getTokenType().concat(" ").concat(response.getToken());
        mockMvc.perform(post("/character")
                .content(characterData)
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isCreated()).andReturn();

        result = mockMvc.perform(get("/character")
                .contentType("application/json")
                .header("Authorization", webToken)
                .param("id", "1"))
                .andExpect(status().isOk()).andReturn();
        mockMvc.perform(post("/character/spellbook")
                .content(spellBookData)
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isCreated()).andReturn();
    }

    @AfterEach
    public void clearUp() throws Exception {
        mockMvc = null;
        characterData = null;
        loginData = null;
        spellBookData = null;
        webToken = null;
    }

    @Test
    public void testCreateSpellBook() throws Exception {
        MvcResult result = mockMvc.perform(get("/character/spellbook")
                .content(spellBookData)
                .param("characterId", "1")
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isOk()).andReturn();
        SpellBookDTO dto = objectMapper.readValue(result.getResponse().getContentAsString(), SpellBookDTO.class);
        Assertions.assertTrue(dto.getCharacterId() != null);
        Assertions.assertTrue(dto.getSpells().size() > 0);

    }

    @Test
    public void testDeleteSpellBook() throws Exception {
        mockMvc.perform(post("/character/spellbook")
                .content(spellBookData)
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isCreated()).andReturn();

        mockMvc.perform(get("/character/spellbook/delete")
                .content(spellBookData)
                .param("characterId", "1")
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isOk()).andReturn();

        MvcResult result = mockMvc.perform(get("/character/spellbook")
                .content(spellBookData)
                .param("characterId", "1")
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isNotFound()).andReturn();


    }


}
