package com.dnd.app;

import com.dnd.app.dto.security.AuthResponseDTO;
import com.dnd.app.dto.security.LoginRequestDTO;
import com.dnd.app.errorHandling.exceptions.BadRequestException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:test.properties")
class AuthorizationTest {

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    private String createdUser;
    private String newUser;

    @BeforeEach
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
        File testData = new File("src/test/resources/json/auth.json");
        createdUser = objectMapper.writeValueAsString(objectMapper.readValue(testData, LoginRequestDTO.class));
        testData = new File("src/test/resources/json/authNew.json");
        newUser = objectMapper.writeValueAsString(objectMapper.readValue(testData, LoginRequestDTO.class));
    }

    @Test
    public void testWhenLoginIsSuccessful() throws Exception {
        MvcResult result = mockMvc.perform(post("/user/singin")
                .content(createdUser)
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        AuthResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDTO.class);
        Assertions.assertNotNull(response.getToken());
        Assertions.assertEquals("Bearer", response.getTokenType());
    }

    @Test
    public void testWhenUserDoesNotExistLoginFails() throws Exception {
        MvcResult result = mockMvc.perform(post("/user/singin")
                .content(newUser)
                .contentType("application/json"))
                .andExpect(status().isBadRequest()).andReturn();
        Assertions.assertEquals(BadRequestException.class, result.getResolvedException().getClass());
    }


    @Test
    public void testWhenCreateUserIsSuccessful() throws Exception {
        MvcResult result = mockMvc.perform(post("/user/create")
                .content(newUser)
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void testWhenCreateUserAlreadyExistsAndFails() throws Exception {
        MvcResult result = mockMvc.perform(post("/user/create")
                .content(createdUser)
                .contentType("application/json"))
                .andExpect(status().isBadRequest()).andReturn();
        Assertions.assertEquals(BadRequestException.class, result.getResolvedException().getClass());
    }

}
