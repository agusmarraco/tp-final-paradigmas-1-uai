package com.dnd.app;

import com.dnd.app.dto.character.CharacterDTO;
import com.dnd.app.dto.security.AuthResponseDTO;
import com.dnd.app.dto.security.LoginRequestDTO;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.io.File;
import java.util.Set;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:test.properties")
class CharacterTest {

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    private String characterData;
    private String webToken;
    private String loginData;

    @BeforeEach
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
        File testData = new File("src/test/resources/json/character.json");
        characterData = objectMapper.writeValueAsString(objectMapper.readValue(testData, CharacterDTO.class));
        testData = new File("src/test/resources/json/auth.json");
        loginData = objectMapper.writeValueAsString(objectMapper.readValue(testData, LoginRequestDTO.class));
    }

    @Test
    public void testWhenUserCreatesCharacterAndCharacterItCreatesItOk() throws Exception {
        MvcResult result = mockMvc.perform(post("/user/singin")
                .content(loginData)
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        AuthResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDTO.class);
        webToken = response.getTokenType().concat(" ").concat(response.getToken());
        mockMvc.perform(post("/character")
                .content(characterData)
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isCreated()).andReturn();

        result = mockMvc.perform(get("/character")
                .contentType("application/json")
                .header("Authorization", webToken)
                .param("id", "1"))
                .andExpect(status().isOk()).andReturn();
        CharacterDTO characterCreated = objectMapper.readValue(result.getResponse().getContentAsString(), CharacterDTO.class);
        Assertions.assertNotNull(characterCreated);
        Set<ConstraintViolation<CharacterDTO>> errors = Validation.buildDefaultValidatorFactory().getValidator().validate(characterCreated);
        Assertions.assertTrue(errors.isEmpty());
    }

    @Test
    public void testWhenUserCreatesCharacterAndIsNotAuthenticatedThenItFails() throws Exception {
        MvcResult result = mockMvc.perform(post("/character")
                .content(characterData)
                .contentType("application/json"))
                .andExpect(status().isUnauthorized()).andReturn();
    }


    @Test
    public void testWhenUserUpdatesCharacterAndIsAuthenticatedThenItRunsOk() throws Exception {
        // TOKEN
        MvcResult result = mockMvc.perform(post("/user/singin")
                .content(loginData)
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        AuthResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDTO.class);
        webToken = response.getTokenType().concat(" ").concat(response.getToken());

        //CREATE
        String newIdResponse = mockMvc.perform(post("/character")
                .content(characterData)
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isCreated()).andReturn().getResponse().getContentAsString();
        String newId = objectMapper.readValue(newIdResponse, JsonNode.class).get("id").toString();
        //GET
        result = mockMvc.perform(get("/character")
                .contentType("application/json")
                .header("Authorization", webToken)
                .param("id", newId))
                .andExpect(status().isOk()).andReturn();

        //UPDATE
        CharacterDTO characterCreated = objectMapper.readValue(result.getResponse().getContentAsString(), CharacterDTO.class);
        characterCreated.setBackground("testarino");
        mockMvc.perform(post("/character")
                .content(objectMapper.writeValueAsString(characterCreated))
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isCreated()).andReturn();

        //GET
        result = mockMvc.perform(get("/character")
                .contentType("application/json")
                .header("Authorization", webToken)
                .param("id", newId))
                .andExpect(status().isOk()).andReturn();

        CharacterDTO characterUpdated = objectMapper.readValue(result.getResponse().getContentAsString(), CharacterDTO.class);

        Assertions.assertNotNull(characterUpdated);
        Set<ConstraintViolation<CharacterDTO>> errors = Validation.buildDefaultValidatorFactory().getValidator().validate(characterCreated);
        Assertions.assertTrue(errors.isEmpty());
        Assertions.assertEquals("testarino", characterUpdated.getBackground());
    }

    @Test
    public void testWhenUserDeletesCharacterAndIsAuthenticatedThenItDeletesIt() throws Exception {
        MvcResult result = mockMvc.perform(post("/user/singin")
                .content(loginData)
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        AuthResponseDTO response = objectMapper.readValue(result.getResponse().getContentAsString(), AuthResponseDTO.class);
        webToken = response.getTokenType().concat(" ").concat(response.getToken());
        mockMvc.perform(post("/character")
                .content(characterData)
                .contentType("application/json")
                .header("Authorization", webToken))
                .andExpect(status().isCreated()).andReturn();

        result = mockMvc.perform(get("/character")
                .contentType("application/json")
                .header("Authorization", webToken)
                .param("id", "1"))
                .andExpect(status().isOk()).andReturn();
        CharacterDTO characterCreated = objectMapper.readValue(result.getResponse().getContentAsString(), CharacterDTO.class);


        result = mockMvc.perform(get("/character/delete")
                .contentType("application/json")
                .header("Authorization", webToken)
                .param("id", "1"))
                .andExpect(status().isOk()).andReturn();
        result = mockMvc.perform(get("/character")
                .contentType("application/json")
                .header("Authorization", webToken)
                .param("id", "1"))
                .andExpect(status().isNotFound()).andReturn();
    }


}
