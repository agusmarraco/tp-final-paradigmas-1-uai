package com.dnd.app;

import com.dnd.app.dto.configuration.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:test.properties")
class ConfigDataTest {

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @AfterEach
    public void clearUp() throws Exception {
        mockMvc = null;
    }

    @Test
    public void testRaces() throws Exception {
        String result = mockMvc.perform(get("/public/config/races")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<RaceDTO> raceDTOS = Arrays.asList(objectMapper.readValue(result, RaceDTO[].class));
        Assertions.assertFalse(raceDTOS.isEmpty());

    }

    @Test
    public void testAbilities() throws Exception {
        String result = mockMvc.perform(get("/public/config/abilities")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<AbilityDTO> abilities = Arrays.asList(objectMapper.readValue(result, AbilityDTO[].class));
        Assertions.assertFalse(abilities.isEmpty());
    }

    @Test
    public void testLanguages() throws Exception {
        String result = mockMvc.perform(get("/public/config/languages")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<LanguageDTO> list = Arrays.asList(objectMapper.readValue(result, LanguageDTO[].class));
        Assertions.assertFalse(list.isEmpty());
    }


    @Test
    public void testSpells() throws Exception {
        String result = mockMvc.perform(get("/public/config/spells")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<SpellDTO> list = Arrays.asList(objectMapper.readValue(result, SpellDTO[].class));
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    public void testSkills() throws Exception {
        String result = mockMvc.perform(get("/public/config/skills")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<SkillDTO> list = Arrays.asList(objectMapper.readValue(result, SkillDTO[].class));
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    public void testClasses() throws Exception {
        String result = mockMvc.perform(get("/public/config/classes")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<ClassTemplateDTO> list = Arrays.asList(objectMapper.readValue(result, ClassTemplateDTO[].class));
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    public void testEquipmentProEfficiencies() throws Exception {
        String result = mockMvc.perform(get("/public/config/equipmentProEfficiencies")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<EquipmentProEfficiencyTypeDTO> list = Arrays.asList(objectMapper.readValue(result, EquipmentProEfficiencyTypeDTO[].class));
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    public void testProEfficiencies() throws Exception {
        String result = mockMvc.perform(get("/public/config/proEfficiencies")
                .contentType("application/json"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
        List<ProEfficiencyDTO> list = Arrays.asList(objectMapper.readValue(result, ProEfficiencyDTO[].class));
        Assertions.assertFalse(list.isEmpty());
    }


}
