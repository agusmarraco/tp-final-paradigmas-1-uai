package com.dnd.app.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public Object errorPage() {
        return "ups";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Object hola() {
        return "hola";
    }
}
