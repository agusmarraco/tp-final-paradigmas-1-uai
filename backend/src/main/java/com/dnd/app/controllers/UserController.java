package com.dnd.app.controllers;

import com.dnd.app.dto.security.AuthResponseDTO;
import com.dnd.app.dto.security.LoginRequestDTO;
import com.dnd.app.entities.user.User;
import com.dnd.app.errorHandling.exceptions.BadRequestException;
import com.dnd.app.security.JwtTokenProvider;
import com.dnd.app.services.user.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserService userService;
    private AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider tokenProvider;

    public UserController(UserService userService, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createUser(@RequestBody LoginRequestDTO loginRequestDTO) {
        ResponseEntity response;
        if (userService.existsByUserName(loginRequestDTO.getUsername())) {
            throw new BadRequestException("user " + loginRequestDTO.getUsername() + " already exists");
        }
        User user = new User(loginRequestDTO.getUsername(), loginRequestDTO.getPassword());
        userService.saveUser(user);
        return ResponseEntity.ok().build();

    }

    @RequestMapping(value = "/singin", method = RequestMethod.POST)
    public ResponseEntity<?> singIn(@RequestBody LoginRequestDTO loginRequestDTO) {
        if (!userService.existsByUserName(loginRequestDTO.getUsername())) {
            throw new BadRequestException("user " + loginRequestDTO.getUsername() + " does not exist");
        }
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDTO.getUsername(), loginRequestDTO.getPassword())
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new AuthResponseDTO(token));

    }


}
