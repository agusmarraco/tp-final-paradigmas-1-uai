package com.dnd.app.controllers;

import com.dnd.app.dto.character.CharacterDTO;
import com.dnd.app.dto.character.SpellBookDTO;
import com.dnd.app.entities.character.Character;
import com.dnd.app.security.CustomPrincipal;
import com.dnd.app.services.character.CharacterService;
import com.dnd.app.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/character")
public class CharacterController {

    private CharacterService characterService;
    private UserService userService;

    @Autowired
    public CharacterController(CharacterService characterService, UserService userService) {
        this.characterService = characterService;
        this.userService = userService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public CharacterDTO getCharacter(@RequestParam Long id) {
        return characterService.getCharacter(id);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity postCharacter(@RequestBody CharacterDTO characterDTO) {
        CustomPrincipal principal = (CustomPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Character character = characterService.saveCharacter(characterDTO, principal.getId());
        Map<String, Long> body = new HashMap<>();
        body.put("id", character.getId());
        ResponseEntity responseEntity = new ResponseEntity(body, HttpStatus.CREATED);
        return responseEntity;
    }

    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ResponseEntity deleteCharacter(@RequestParam Long id) {

        characterService.deleteCharacter(id);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/spellbook", method = RequestMethod.GET)
    public SpellBookDTO getSpellBook(@RequestParam Long characterId) {
        return characterService.getSpellBook(characterId);
    }

    @RequestMapping(value = "/spellbook", method = RequestMethod.POST)
    public ResponseEntity postSpellBook(@RequestBody SpellBookDTO spellBookDTO) {
        characterService.saveSpellBook(spellBookDTO);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/spellbook/delete", method = RequestMethod.GET)
    public ResponseEntity deleteSpellBook(@RequestParam Long characterId) {
        characterService.deleteSpellBook(characterId);
        return ResponseEntity.ok().build();
    }


}
