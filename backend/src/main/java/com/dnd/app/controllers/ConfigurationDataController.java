package com.dnd.app.controllers;

import com.dnd.app.dto.configuration.*;
import com.dnd.app.services.config.ConfigurationDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public/config")
public class ConfigurationDataController {

    private ConfigurationDataService configurationDataService;

    @Autowired
    public ConfigurationDataController(ConfigurationDataService configurationDataService) {
        this.configurationDataService = configurationDataService;
    }

    @RequestMapping(value = "/races", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RaceDTO> getRaces() {
        return configurationDataService.findAllRaces();
    }

    @RequestMapping(value = "/abilities", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbilityDTO> getAbilities() {
        return configurationDataService.findAllAbilities();
    }

    @RequestMapping(value = "/languages", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<LanguageDTO> getLanguages() {
        return configurationDataService.findAllLanguages();
    }

    @RequestMapping(value = "/spells", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SpellDTO> getSpells() {
        return configurationDataService.findAllSpells();
    }

    @RequestMapping(value = "/skills", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SkillDTO> getSkills() {
        return configurationDataService.findAllSkills();
    }

    @RequestMapping(value = "/classes", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ClassTemplateDTO> getClasses() {
        return configurationDataService.findAllClasses();
    }

    @RequestMapping(value = "/equipmentProEfficiencies", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<EquipmentProEfficiencyTypeDTO> getEquipmentProEfficiencies() {
        return configurationDataService.findAllEquipmentProEfficienciesRepository();
    }

    @RequestMapping(value = "/proEfficiencies", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ProEfficiencyDTO> getProEfficiencies() {
        return configurationDataService.findAllProEfficiencies();
    }
}
