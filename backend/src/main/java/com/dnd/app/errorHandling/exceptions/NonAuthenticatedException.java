package com.dnd.app.errorHandling.exceptions;

import org.springframework.security.core.AuthenticationException;

public class NonAuthenticatedException extends AuthenticationException {
    public NonAuthenticatedException(String msg) {
        super(msg);
    }
}
