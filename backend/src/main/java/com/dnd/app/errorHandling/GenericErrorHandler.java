package com.dnd.app.errorHandling;

import com.dnd.app.errorHandling.exceptions.BadRequestException;
import com.dnd.app.errorHandling.exceptions.NonAuthenticatedException;
import com.dnd.app.errorHandling.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@ControllerAdvice
public class GenericErrorHandler extends ResponseEntityExceptionHandler {

    private Map<Class, Function<Exception, ResponseEntity>> handlers;


    public GenericErrorHandler() {
        this.handlers = new HashMap<>();
        this.handlers.put(NotFoundException.class, this::handleNotFound);
        this.handlers.put(BadRequestException.class, this::handleBadRequest);
        this.handlers.put(NonAuthenticatedException.class, this::handleNoAuth);
    }

    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        ResponseEntity responseEntity = null;
        if (handlers.containsKey(ex.getClass())) {
            responseEntity = handlers.get(ex.getClass()).apply(ex);
        } else {
            responseEntity = handleDefault(ex);
        }
        return responseEntity;
    }

    private ResponseEntity handleNotFound(Exception ex) {
        Map<String, String> body = new HashMap<>();
        body.put("message", ex.getMessage());
        ResponseEntity responseEntity = new ResponseEntity(body, HttpStatus.NOT_FOUND);
        return responseEntity;
    }

    private ResponseEntity handleBadRequest(Exception ex) {
        Map<String, String> body = new HashMap<>();
        body.put("message", ex.getMessage());
        ResponseEntity responseEntity = new ResponseEntity(body, HttpStatus.BAD_REQUEST);
        return responseEntity;
    }

    private ResponseEntity handleNoAuth(Exception ex) {
        Map<String, String> body = new HashMap<>();
        body.put("message", ex.getMessage());
        ResponseEntity responseEntity = new ResponseEntity(body, HttpStatus.UNAUTHORIZED);
        return responseEntity;
    }

    private ResponseEntity handleDefault(Exception ex) {
        Map<String, String> body = new HashMap<>();
        body.put("message", ex.getMessage());
        ResponseEntity responseEntity = new ResponseEntity(body, HttpStatus.INTERNAL_SERVER_ERROR);
        return responseEntity;
    }
}
