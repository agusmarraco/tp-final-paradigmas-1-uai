package com.dnd.app.builders;

import com.dnd.app.entities.configuration.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class RaceBuilder {

    public static List<Race> buildRaces() {
        List<Race> races = new ArrayList<>();
        races.add(buildDragonBorn());
        races.add(buildHalfOrc());
        return races;
    }

    private static Race buildDragonBorn() {
        Set<Ability> abilities = AbilityBuilder.buildAbilities();
        Set<Trait> traits = new HashSet<>();

        Set<Language> languages = LanguageBuilder.buildLanguages().stream().filter(language -> language.getName().equalsIgnoreCase("common")
                || language.getName().equalsIgnoreCase("draconic")).collect(Collectors.toSet());


        Trait trait = new Trait("Damage Resistance", "You have resistance to the damage type associated with your draconic ancestry");
        traits.add(trait);

        Race race = new Race("DragonBorn", 30l, languages, traits);
        Ability ability = abilities.stream().filter(x -> x.getCode().equals("STR")).findFirst().get();
        ScoreIncrease scoreIncrease1 = new ScoreIncrease(race, ability, 2l);

        ability = abilities.stream().filter(x -> x.getCode().equals("CHA")).findFirst().get();
        ScoreIncrease scoreIncrease2 = new ScoreIncrease(race, ability, 1l);

        race.setScoreIncreases(Set.of(scoreIncrease1, scoreIncrease2));
        return race;
    }

    private static Race buildHalfOrc() {
        Set<Ability> abilities = AbilityBuilder.buildAbilities();
        Set<Trait> traits = new HashSet<>();

        Set<Language> languages = LanguageBuilder.buildLanguages().stream()
                .filter(language -> language.getName().equalsIgnoreCase("common")
                        || language.getName().equalsIgnoreCase("orc")).collect(Collectors.toSet());
        Trait trait = new Trait("Darkvision", "Thanks to your orc blood, you have superior vision in dark and dim conditions. You can see in dim light within 60 feet of you as if it were bright light, and in darkness as if it were dim light. You can’t discern color in darkness, only shades of gray.");
        Trait trait1 = new Trait("Menacing", "You gain proficiency in the Intimidation skill.");
        Trait trait2 = new Trait("Relentless Endurance", "When you are reduced to 0 hit points but not killed outright, you can drop to 1 hit point instead. You can’t use this feature again until you finish a long rest.");
        Trait trait3 = new Trait("Savage Attacks", "When you score a critical hit with a melee weapon attack, you can roll one of the weapon’s damage dice one additional time and add it to the extra damage of the critical hit.");
        traits.add(trait);
        traits.add(trait1);
        traits.add(trait2);
        traits.add(trait3);

        Race race = new Race("HalfOrc", 30l, languages, traits);
        Ability ability = abilities.stream().filter(x -> x.getCode().equals("STR")).findFirst().get();
        ScoreIncrease scoreIncrease1 = new ScoreIncrease(race, ability, 2l);

        ability = abilities.stream().filter(x -> x.getCode().equals("CON")).findFirst().get();
        ScoreIncrease scoreIncrease2 = new ScoreIncrease(race, ability, 1l);

        race.setScoreIncreases(Set.of(scoreIncrease1, scoreIncrease2));
        return race;
    }
}
