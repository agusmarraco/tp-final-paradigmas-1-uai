package com.dnd.app.builders;

import com.dnd.app.entities.configuration.Ability;

import java.util.HashSet;
import java.util.Set;

public class AbilityBuilder {

    private static Set<Ability> abilities;

    public static Set<Ability> buildAbilities() {
        if (abilities == null) {
            abilities = new HashSet<>();
            abilities.add(new Ability("STR", "STRENGTH"));
            abilities.add(new Ability("DEX", "DEXTERITY"));
            abilities.add(new Ability("CON", "CONSTITUTION"));
            abilities.add(new Ability("INT", "INTELLIGENCE"));
            abilities.add(new Ability("WIS", "WISDOM"));
            abilities.add(new Ability("CHA", "CHARISMA"));
        }
        return abilities;
    }
}
