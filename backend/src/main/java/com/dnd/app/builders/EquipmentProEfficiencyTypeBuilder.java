package com.dnd.app.builders;

import com.dnd.app.entities.configuration.EquipmentProEfficiencyType;

import java.util.HashSet;
import java.util.Set;

public class EquipmentProEfficiencyTypeBuilder {

    private static Set<EquipmentProEfficiencyType> equipmentProEfficiencyTypes;

    public static Set<EquipmentProEfficiencyType> buildEquipmentProEfficiencies() {
        if (equipmentProEfficiencyTypes == null) {
            equipmentProEfficiencyTypes = new HashSet<>();
            equipmentProEfficiencyTypes.add(new EquipmentProEfficiencyType("ARMOR"));
            equipmentProEfficiencyTypes.add(new EquipmentProEfficiencyType("WEAPONS"));
            equipmentProEfficiencyTypes.add(new EquipmentProEfficiencyType("TOOLS"));

        }
        return equipmentProEfficiencyTypes;
    }
}
