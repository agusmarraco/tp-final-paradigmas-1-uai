package com.dnd.app.builders;

import com.dnd.app.entities.configuration.Ability;
import com.dnd.app.entities.configuration.Skill;

import java.util.HashSet;
import java.util.Set;

public class SkillBuilder {

    private static Set<Skill> skills;

    public static Set<Skill> buildSkills() {
        Set<Ability> abilities = AbilityBuilder.buildAbilities();
        if (skills == null) {
            skills = new HashSet<>();

            for (Ability ability : abilities) {
                switch (ability.getCode()) {
                    case "STR":
                        skills.add(new Skill("Athletics", ability));
                        break;
                    case "DEX":
                        skills.add(new Skill("Acrobatics", ability));
                        skills.add(new Skill("Sleight Of Hand", ability));
                        skills.add(new Skill("Stealth", ability));
                        break;
                    case "INT":
                        skills.add(new Skill("Arcana", ability));
                        skills.add(new Skill("History", ability));
                        skills.add(new Skill("Investigation", ability));
                        skills.add(new Skill("Nature", ability));
                        skills.add(new Skill("Religion", ability));
                        break;
                    case "WIS":
                        skills.add(new Skill("Animal Handling", ability));
                        skills.add(new Skill("Insight", ability));
                        skills.add(new Skill("Medicine", ability));
                        skills.add(new Skill("Perception", ability));
                        skills.add(new Skill("Survival", ability));
                        break;
                    case "CHA":
                        skills.add(new Skill("Deception", ability));
                        skills.add(new Skill("Intimidation", ability));
                        skills.add(new Skill("Performance", ability));
                        skills.add(new Skill("Persuasion", ability));
                        break;
                    default:

                }
            }
        }

        return skills;
    }
}
