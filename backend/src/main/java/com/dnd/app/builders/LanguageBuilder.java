package com.dnd.app.builders;

import com.dnd.app.entities.configuration.Language;

import java.util.HashSet;
import java.util.Set;

public class LanguageBuilder {

    private static Set<Language> languages;

    public static Set<Language> buildLanguages() {
        if (languages == null) {
            languages = new HashSet<>();
            languages.add(new Language("Elvish"));
            languages.add(new Language("Common"));
            languages.add(new Language("Draconic"));
            languages.add(new Language("Orc"));
        }

        return languages;
    }
}
