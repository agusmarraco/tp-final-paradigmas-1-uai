package com.dnd.app.builders;

import com.dnd.app.entities.configuration.Spell;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class SpellBuilder {

    private static String filePath = "json/spells.json";
    private static Set<Spell> spells;

    public static Set<Spell> buildSpells(ObjectMapper objectMapper) {
        File file = new File(SpellBuilder.class.getClassLoader().getResource(filePath).getFile());
        try {
            if (spells == null) {
                spells = objectMapper.readValue(file, new TypeReference<Set<Spell>>() {
                });

            }
            return spells;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
