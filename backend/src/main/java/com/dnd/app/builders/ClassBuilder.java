package com.dnd.app.builders;

import com.dnd.app.entities.configuration.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ClassBuilder {

    public static Set<ClassTemplate> buildClasses(ObjectMapper objectMapper) {
        Set<ClassTemplate> classes = new HashSet<>();
        classes.add(barbarianBuilder());
        classes.add(wizardBuilder(objectMapper));
        return classes;
    }

    private static ClassTemplate barbarianBuilder() {
        Set<Ability> abilities = AbilityBuilder.buildAbilities();
        ClassTemplate barb = new ClassTemplate();
        barb.setName("Barbarian");
        SavingThrow savingThrow = new SavingThrow(abilities.stream().filter(ability -> ability.getCode().equals("STR")).findFirst().get());
        SavingThrow savingThrow2 = new SavingThrow(abilities.stream().filter(ability -> ability.getCode().equals("CON")).findFirst().get());
        barb.setSavingThrows(Arrays.asList(savingThrow, savingThrow2));

        Trait trait = new Trait("Rage", "In battle, you fight with primal ferocity. On your turn, you can enter a rage as a bonus action." +
                "While raging, you gain the following benefits if you aren’t wearing heavy armor:" +
                "You have advantage on Strength checks and Strength saving throws." +
                "When you make a melee weapon attack using Strength, you gain a bonus to the damage roll that increases as you gain levels as a barbarian, as shown in the Rage Damage column of the Barbarian table." +
                "You have resistance to bludgeoning, piercing, and slashing damage." +
                "If you are able to cast spells, you can’t cast them or concentrate on them while raging." +
                "Your rage lasts for 1 minute. It ends early if you are knocked unconscious or if your turn ends and you haven’t attacked a hostile creature since your last turn or taken damage since then. You can also end your rage on your turn as a bonus action." +
                "Once you have raged the number of times shown for your barbarian level in the Rages column of the Barbarian table, you must finish a long rest before you can rage again.");
        Trait trait1 = new Trait("Unarmored Defense", "While you are not wearing any armor, your Armor Class equals 10 + your Dexterity modifier + your Constitution modifier. You can use a shield and still gain this benefit.");
        Trait trait2 = new Trait("Reckless Attack", "Starting at 2nd level, you can throw aside all concern for defense to attack with fierce desperation. When you make your first attack on your turn, you can decide to attack recklessly. Doing so gives you advantage on melee weapon attack rolls using Strength during this turn, but attack rolls against you have advantage until your next turn.");
        Trait trait3 = new Trait("Danger Sense", "At 2nd level, you gain an uncanny sense of when things nearby aren’t as they should be, giving you an edge when you dodge away from danger." + "You have advantage on Dexterity saving throws against effects that you can see, such as traps and spells. To gain this benefit, you can’t be blinded, deafened, or incapacitated.");
        Trait trait4 = new Trait("Extra Attack", "Beginning at 5th level, you can attack twice, instead of once, whenever you take the Attack action on your turn.");
        Trait trait5 = new Trait("Fast Movement", "Starting at 5th level, your speed increases by 10 feet while you aren’t wearing heavy armor.");
        Trait trait6 = new Trait("Feral Instinct", "By 7th level, your instincts are so honed that you have advantage on initiative rolls. Additionally, if you are surprised at the beginning of combat and aren’t incapacitated, you can act normally on your first turn, but only if you enter your rage before doing anything else on that turn.");
        Trait trait7 = new Trait("Brutal Critical", "Beginning at 9th level, you can roll one additional weapon damage die when determining the extra damage for a critical hit with a melee attack. This increases to two additional dice at 13th level and three additional dice at 17th level.");
        Trait trait8 = new Trait("Relentless Rage", "Starting at 11th level, your rage can keep you fighting despite grievous wounds. If you drop to 0 hit points while you’re raging and don’t die outright, you can make a DC 10 Constitution saving throw. If you succeed, you drop to 1 hit point instead. Each time you use this feature after the first, the DC increases by 5. When you finish a short or long rest, the DC resets to 10.");
        Trait trait9 = new Trait("Persistent Rage", "Beginning at 15th level, your rage is so fierce that it ends early only if you fall unconscious or if you choose to end it.");
        Trait trait10 = new Trait("Indomitable Might", "Beginning at 18th level, if your total for a Strength check is less than your Strength score, you can use that score in place of the total.");
        Trait trait11 = new Trait("Primal Champion", "At 20th level, you embody the power of the wilds. Your Strength and Constitution scores increase by 4. Your maximum for those scores is now 24.");
        barb.setTraits(Arrays.asList(trait, trait1, trait2, trait3, trait4, trait5, trait6, trait7, trait8, trait9, trait10, trait11));

        EquipmentProEfficiencyType armor = EquipmentProEfficiencyTypeBuilder.buildEquipmentProEfficiencies().stream().filter(type -> type.getType().equals("ARMOR")).findFirst().get();
        EquipmentProEfficiencyType weapons = EquipmentProEfficiencyTypeBuilder.buildEquipmentProEfficiencies().stream().filter(type -> type.getType().equals("WEAPONS")).findFirst().get();
        ProEfficiency proEfficiency = new ProEfficiency("Light Armor", armor);
        ProEfficiency proEfficiency1 = new ProEfficiency("Medium Armor", armor);
        ProEfficiency proEfficiency2 = new ProEfficiency("Shield", armor);
        ProEfficiency proEfficiency3 = new ProEfficiency("Simple Weapon", weapons);
        ProEfficiency proEfficiency4 = new ProEfficiency("Martial Weapon", weapons);
        barb.setProEfficiencies(Arrays.asList(proEfficiency, proEfficiency1, proEfficiency2, proEfficiency3, proEfficiency4));

        Set<Skill> skills = SkillBuilder.buildSkills();
        List<String> barbSkills = Arrays.asList("animal handling", "athletics", "intimidation", "nature", "perception", "survival");
        barb.setSkills(skills.stream().filter(skill -> barbSkills.contains(skill.getName().toLowerCase())).collect(Collectors.toList()));

        return barb;
    }

    private static ClassTemplate wizardBuilder(ObjectMapper objectMapper) {
        Set<Ability> abilities = AbilityBuilder.buildAbilities();
        ClassTemplate wizard = new ClassTemplate();
        wizard.setName("Wizard");
        SavingThrow savingThrow = new SavingThrow(abilities.stream().filter(ability -> ability.getCode().equals("INT")).findFirst().get());
        SavingThrow savingThrow2 = new SavingThrow(abilities.stream().filter(ability -> ability.getCode().equals("WIS")).findFirst().get());
        wizard.setSavingThrows(Arrays.asList(savingThrow, savingThrow2));

        List<Trait> traits = new ArrayList<>();
        traits.add(new Trait("Arcane Recovery", "You have learned to regain some of your magical energy by studying your spellbook. Once per day when you finish a short rest, you can choose expended spell slots to recover. The spell slots can have a combined level that is equal to or less than half your wizard level (rounded up), and none of the slots can be 6th level or higher. For example, if you’re a 4th-level wizard, you can recover up to two levels worth of spell slots. You can recover either a 2nd-level spell slot or two 1st-level spell slots."));
        traits.add(new Trait("Arcane Tradition", "When you reach 2nd level, you choose an arcane tradition, shaping your practice of magic through one of eight schools: Abjuration, Conjuration, Divination, Enchantment, Evocation, Illusion, Necromancy, or Transmutation, all detailed at the end of the class description. Your choice grants you features at 2nd level and again at 6th, 10th, and 14th level."));
        traits.add(new Trait("Ability Score Improvement", "When you reach 4th level, and again at 8th, 12th, 16th, and 19th level, you can increase one ability score of your choice by 2, or you can increase two ability scores of your choice by 1. As normal, you can’t increase an ability score above 20 using this feature."));
        traits.add(new Trait("Spell Mastery", "At 18th level, you have achieved such mastery over certain spells that you can cast them at will. Choose a 1st-level wizard spell and a 2nd-level wizard spell that are in your spellbook. You can cast those spells at their lowest level without expending a spell slot when you have them prepared. If you want to cast either spell at a higher level, you must expend a spell slot as normal. By spending 8 hours in study, you can exchange one or both of the spells you chose for different spells of the same levels."));
        traits.add(new Trait("Signature Spells", "When you reach 20th level, you gain mastery over two powerful spells and can cast them with little effort. Choose two 3rd-level wizard spells in your spellbook as your signature spells. You always have these spells prepared, they don’t count against the number of spells you have prepared, and you can cast each of them once at 3rd level without expending a spell slot. When you do so, you can’t do so again until you finish a short or long rest. If you want to cast either spell at a higher level, you must expend a spell slot as normal."));
        wizard.setTraits(traits);

        EquipmentProEfficiencyType weapons = EquipmentProEfficiencyTypeBuilder.buildEquipmentProEfficiencies().stream().filter(type -> type.getType().equals("WEAPONS")).findFirst().get();
        ProEfficiency proEfficiency = new ProEfficiency("Simple Weapon", weapons);
        wizard.setProEfficiencies(Arrays.asList(proEfficiency));
        Set<Skill> skills = SkillBuilder.buildSkills();
        List<String> wizardSkills = Arrays.asList("arcana", "history", "insight", "investigation", "medicine", "religion");
        wizard.setSkills(skills.stream().filter(skill -> wizardSkills.contains(skill.getName().toLowerCase())).collect(Collectors.toList()));
        wizard.setSpellCastingAbility(abilities.stream().filter(ability -> ability.getCode().equals("INT")).findFirst().get());

        File file = new File(SpellBuilder.class.getClassLoader().getResource("json/wizardSpellTable.json").getFile());
        try {
            List<SpellSlotPerClass> spellSlotPerClasses = objectMapper.readValue(file, new TypeReference<List<SpellSlotPerClass>>() {
            });
            spellSlotPerClasses.forEach(spellSlotPerClass -> spellSlotPerClass.setClassTemplate(wizard));
            wizard.setSpellSlotPerClass(spellSlotPerClasses);
        } catch (IOException e) {
            new RuntimeException(e);
        }
        return wizard;
    }

}