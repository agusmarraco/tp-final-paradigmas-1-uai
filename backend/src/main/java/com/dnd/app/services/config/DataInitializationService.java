package com.dnd.app.services.config;

import com.dnd.app.builders.*;
import com.dnd.app.entities.user.User;
import com.dnd.app.repositories.config.*;
import com.dnd.app.repositories.user.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataInitializationService {

    private AbilityRepository abilityRepository;

    private ClassTemplateRepository classTemplateRepository;

    private EquipmentProEfficiencyTypeRepository equipmentProEfficiencyTypeRepository;

    private LanguageRepository languageRepository;


    private RaceRepository raceRepository;


    private SkillRepository skillRepository;

    private SpellRepository spellRepository;

    private ObjectMapper objectMapper;

    private UserRepository userRepository;

    @Autowired
    public DataInitializationService(AbilityRepository abilityRepository, ClassTemplateRepository classTemplateRepository,
                                     EquipmentProEfficiencyTypeRepository equipmentProEfficiencyTypeRepository, LanguageRepository languageRepository,
                                     RaceRepository raceRepository, SkillRepository skillRepository, SpellRepository spellRepository, ObjectMapper objectMapper,
                                     UserRepository userRepository) {
        this.abilityRepository = abilityRepository;
        this.classTemplateRepository = classTemplateRepository;
        this.equipmentProEfficiencyTypeRepository = equipmentProEfficiencyTypeRepository;
        this.languageRepository = languageRepository;
        this.raceRepository = raceRepository;
        this.skillRepository = skillRepository;
        this.spellRepository = spellRepository;
        this.objectMapper = objectMapper;
        this.userRepository = userRepository;
    }

    public void initDatabaseData() {
        abilityRepository.saveAll(AbilityBuilder.buildAbilities());
        languageRepository.saveAll(LanguageBuilder.buildLanguages());
        skillRepository.saveAll(SkillBuilder.buildSkills());
        equipmentProEfficiencyTypeRepository.saveAll(EquipmentProEfficiencyTypeBuilder.buildEquipmentProEfficiencies());
        spellRepository.saveAll(SpellBuilder.buildSpells(objectMapper));
        raceRepository.saveAll(RaceBuilder.buildRaces());
        classTemplateRepository.saveAll(ClassBuilder.buildClasses(objectMapper));
        User user = new User();
        user.setUsername("admin");
        user.setPassword("$2y$12$pG9O/FyH9jID9qc9A6kn6eIFD.GsImkzkn8Ad6ZnUpHJrLPD.kIka");
        userRepository.save(user);
    }

}
