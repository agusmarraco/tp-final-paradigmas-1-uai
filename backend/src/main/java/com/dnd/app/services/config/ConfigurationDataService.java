package com.dnd.app.services.config;

import com.dnd.app.dto.configuration.*;
import com.dnd.app.repositories.character.ProEfficiencyRepository;
import com.dnd.app.repositories.config.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ConfigurationDataService {

    private RaceRepository raceRepository;

    private LanguageRepository languageRepository;

    private AbilityRepository abilityRepository;


    private SpellRepository spellRepository;

    private SkillRepository skillRepository;

    private EquipmentProEfficiencyTypeRepository equipmentProEfficiencyTypeRepository;

    private ClassTemplateRepository classTemplateRepository;
    private ProEfficiencyRepository proEfficiencyRepository;

    @Autowired
    public ConfigurationDataService(RaceRepository raceRepository, LanguageRepository languageRepository, AbilityRepository abilityRepository, SpellRepository spellRepository, SkillRepository skillRepository, EquipmentProEfficiencyTypeRepository equipmentProEfficiencyTypeRepository, ClassTemplateRepository classTemplateRepository, ProEfficiencyRepository proEfficiencyRepository) {
        this.raceRepository = raceRepository;
        this.languageRepository = languageRepository;
        this.abilityRepository = abilityRepository;
        this.spellRepository = spellRepository;
        this.skillRepository = skillRepository;
        this.equipmentProEfficiencyTypeRepository = equipmentProEfficiencyTypeRepository;
        this.classTemplateRepository = classTemplateRepository;
        this.proEfficiencyRepository = proEfficiencyRepository;
    }


    public List<RaceDTO> findAllRaces() {
        return this.raceRepository.findAll().stream().map(x -> new RaceDTO(x)).collect(Collectors.toList());
    }

    public List<LanguageDTO> findAllLanguages() {
        return this.languageRepository.findAll().stream().map(x -> new LanguageDTO(x)).collect(Collectors.toList());
    }

    public List<AbilityDTO> findAllAbilities() {
        return this.abilityRepository.findAll().stream().map(x -> new AbilityDTO(x)).collect(Collectors.toList());
    }

    public List<SpellDTO> findAllSpells() {
        return this.spellRepository.findAll().stream().map(x -> new SpellDTO(x)).collect(Collectors.toList());
    }

    public List<ClassTemplateDTO> findAllClasses() {
        return this.classTemplateRepository.findAll().stream().map(x -> new ClassTemplateDTO(x)).collect(Collectors.toList());
    }

    public List<SkillDTO> findAllSkills() {
        return this.skillRepository.findAll().stream().map(x -> new SkillDTO(x)).collect(Collectors.toList());
    }

    public List<EquipmentProEfficiencyTypeDTO> findAllEquipmentProEfficienciesRepository() {
        return this.equipmentProEfficiencyTypeRepository.findAll().stream().map(x -> new EquipmentProEfficiencyTypeDTO(x)).collect(Collectors.toList());
    }

    public List<ProEfficiencyDTO> findAllProEfficiencies() {
        return this.proEfficiencyRepository.findAll().stream().map(x -> new ProEfficiencyDTO(x)).collect(Collectors.toList());
    }
}
