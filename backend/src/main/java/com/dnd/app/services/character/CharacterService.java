package com.dnd.app.services.character;

import com.dnd.app.dto.character.*;
import com.dnd.app.dto.configuration.ProEfficiencyDTO;
import com.dnd.app.entities.character.Character;
import com.dnd.app.entities.character.*;
import com.dnd.app.entities.configuration.ProEfficiency;
import com.dnd.app.entities.configuration.Spell;
import com.dnd.app.errorHandling.exceptions.NotFoundException;
import com.dnd.app.repositories.character.CharacterRepository;
import com.dnd.app.repositories.character.ProEfficiencyRepository;
import com.dnd.app.repositories.character.SpellBookRepository;
import com.dnd.app.repositories.config.*;
import com.dnd.app.repositories.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CharacterService {

    private CharacterRepository characterRepository;
    private SpellBookRepository spellBookRepository;
    private RaceRepository raceRepository;
    private EquipmentProEfficiencyTypeRepository equipmentProEfficiencyTypeRepository;
    private ClassTemplateRepository classTemplateRepository;
    private SkillRepository skillRepository;
    private AbilityRepository abilityRepository;
    private ProEfficiencyRepository proEfficiencyRepository;
    private UserRepository userRepository;
    private SpellRepository spellRepository;

    @Autowired
    public CharacterService(CharacterRepository characterRepository, SpellBookRepository spellBookRepository, RaceRepository raceRepository,
                            EquipmentProEfficiencyTypeRepository equipmentProEfficiencyTypeRepository, ClassTemplateRepository classTemplateRepository
            , SkillRepository skillRepository, AbilityRepository abilityRepository,
                            SpellRepository spellRepository,
                            ProEfficiencyRepository proEfficiencyRepository, UserRepository userRepository) {
        this.characterRepository = characterRepository;
        this.spellBookRepository = spellBookRepository;
        this.raceRepository = raceRepository;
        this.equipmentProEfficiencyTypeRepository = equipmentProEfficiencyTypeRepository;
        this.classTemplateRepository = classTemplateRepository;
        this.skillRepository = skillRepository;
        this.abilityRepository = abilityRepository;
        this.proEfficiencyRepository = proEfficiencyRepository;
        this.userRepository = userRepository;
        this.spellRepository = spellRepository;
    }

    @Transactional
    public Character saveCharacter(CharacterDTO characterDTO, Long userId) {
        Character character = new Character();
        if (characterDTO.getId() != null) {
            character = characterRepository.findById(characterDTO.getId())
                    .orElseThrow(() -> new NotFoundException("character id " + characterDTO.getId() + " not found"));
        }
        character.setUser(userRepository.findById(userId).orElseThrow(() -> new NotFoundException("user " + userId + " not found")));
        character.setName(characterDTO.getName()).setBackground(characterDTO.getBackground())
                .setLevel(characterDTO.getLevel()).setDescription(characterDTO.getDescription())
                .setProEfficiencyBonus(characterDTO.getProEfficiencyBonus())
                .setHasInspiration(characterDTO.getHasInspiration());
        character.setCharacterHealthAndArmor(new CharacterHealthAndArmor(character, characterDTO.getCharacterHealthAndArmor()));


        character.getEquipment().clear();
        for (EquipmentDTO dto : characterDTO.getEquipment()) {
            Equipment equipment = new Equipment();
            equipment.setAc(dto.getAc()).setCharacter(character).setDescription(dto.getDescription())
                    .setName(dto.getName());
            character.getEquipment().add(equipment);
        }

        for (CharacterSkillDTO dto : characterDTO.getCharacterSkills()) {
            CharacterSkill skill;
            if (dto.getId() == null) {
                skill = new CharacterSkill();
                skill.setSkill(skillRepository.findById(dto.getSkill().getId()).get());
                skill.setCharacter(character);
                character.getCharacterSkills().add(skill);

            } else {
                skill = character.getCharacterSkills().stream().filter(x -> x.getId().equals(dto.getId())).findFirst().get();
            }
            skill.setProEfficient(dto.getProEfficient());
        }


        character.getProEfficiencies().clear();
        for (ProEfficiencyDTO dto : characterDTO.getProEfficiencies()) {
            ProEfficiency proEfficiency = proEfficiencyRepository.findById(dto.getId()).get();
            character.getProEfficiencies().add(proEfficiency);
        }


        for (CharacterAbilityDTO dto : characterDTO.getCharacterAbilities()) {
            CharacterAbility characterAbility;
            if (dto.getId() == null) {
                characterAbility = new CharacterAbility();
                characterAbility.setAbility(abilityRepository.findById(dto.getAbility().getId()).get());
                characterAbility.setCharacter(character);
                character.getCharacterAbilities().add(characterAbility);

            } else {
                characterAbility = character.getCharacterAbilities().stream().filter(x -> x.getId().equals(dto.getId())).findFirst().get();
            }
            characterAbility.setBaseValue(dto.getBaseValue());

            characterAbility.setExternalModifiers(dto.getExternalModifiers());
            characterAbility.setModifier(dto.getModifier());
        }

        character.setRace(raceRepository.findById(characterDTO.getRaceId()).get());
        character.setClassTemplate(classTemplateRepository.findById(characterDTO.getClassId()).get());
        characterRepository.saveAndFlush(character);
        return character;
    }


    @Transactional
    public CharacterDTO getCharacter(Long characterId) {
        return new CharacterDTO(characterRepository.findById(characterId).orElseThrow(() -> new NotFoundException("character with id: " + characterId + " not found")));
    }

    @Transactional
    public void deleteCharacter(Long characterId) {
        spellBookRepository.deleteByCharacterId(characterId);
        characterRepository.deleteById(characterId);
    }


    @Transactional
    public SpellBookDTO getSpellBook(Long characterId) {
        SpellBook spellBook = spellBookRepository.findByCharacterId(characterId).orElseThrow(() ->
                new NotFoundException("spellbook not found for character: " + characterId));
        SpellBookDTO dto = new SpellBookDTO(spellBook);
        return dto;
    }

    @Transactional
    public void deleteSpellBook(Long characterId) {
        spellBookRepository.deleteByCharacterId(characterId);
    }

    @Transactional
    public void saveSpellBook(SpellBookDTO spellBookDTO) {
        SpellBook spellBook = new SpellBook();
        if (spellBookDTO.getId() != null) {
            spellBook = spellBookRepository.findById(spellBookDTO.getId()).get();
        }
        spellBook.getSpells().clear();
        List<Long> ids = spellBookDTO.getSpells().stream().map(spellDTO -> spellDTO.getId()).collect(Collectors.toList());
        List<Spell> spells = spellRepository.findAllById(ids);
        spellBook.getSpells().addAll(spells);
        if (spellBook.getCharacter() == null) {
            spellBook.setCharacter(characterRepository.findById(spellBookDTO.getCharacterId()).get());
        }
        spellBookRepository.saveAndFlush(spellBook);
    }
}
