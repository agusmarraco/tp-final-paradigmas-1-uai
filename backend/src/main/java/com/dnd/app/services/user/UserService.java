package com.dnd.app.services.user;

import com.dnd.app.entities.user.User;
import com.dnd.app.errorHandling.exceptions.NotFoundException;
import com.dnd.app.repositories.user.UserRepository;
import com.dnd.app.security.CustomPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new NotFoundException("Username not found: " + username);
        }
        return new CustomPrincipal(user.getId(), user.getUsername(), user.getPassword());
    }


    public boolean existsByUserName(String username) {
        User user = userRepository.findByUsername(username);
        return user != null;
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }

    public User findUserById(Long userId) {
        return userRepository.findById(userId).get();
    }
}
