package com.dnd.app.entities.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
public class SpellSlotPerClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "classTemplate")
    private ClassTemplate classTemplate;

    @Column
    @JsonProperty("level")
    private Long level;

    @Column
    @JsonProperty("level0Slot")
    private Long level0Slot;

    @Column
    @JsonProperty("level1Slot")
    private Long level1Slot;

    @Column
    @JsonProperty("level2Slot")
    private Long level2Slot;

    @Column
    @JsonProperty("level3Slot")
    private Long level3Slot;

    @Column
    @JsonProperty("level4Slot")
    private Long level4Slot;

    @Column
    @JsonProperty("level5Slot")
    private Long level5Slot;

    @Column
    @JsonProperty("level6Slot")
    private Long level6Slot;

    @Column
    @JsonProperty("level7Slot")
    private Long level7Slot;

    @Column
    @JsonProperty("level8Slot")
    private Long level8Slot;

    @Column
    @JsonProperty("level9Slot")
    private Long level9Slot;

    public SpellSlotPerClass() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClassTemplate getClassTemplate() {
        return classTemplate;
    }

    public void setClassTemplate(ClassTemplate classTemplate) {
        this.classTemplate = classTemplate;
    }

    public Long getLevel() {
        return level;
    }

    public Long getLevel0Slot() {
        return level0Slot;
    }

    public Long getLevel1Slot() {
        return level1Slot;
    }

    public Long getLevel2Slot() {
        return level2Slot;
    }

    public Long getLevel3Slot() {
        return level3Slot;
    }

    public Long getLevel4Slot() {
        return level4Slot;
    }

    public Long getLevel5Slot() {
        return level5Slot;
    }

    public Long getLevel6Slot() {
        return level6Slot;
    }

    public Long getLevel7Slot() {
        return level7Slot;
    }

    public Long getLevel8Slot() {
        return level8Slot;
    }

    public Long getLevel9Slot() {
        return level9Slot;
    }
}
