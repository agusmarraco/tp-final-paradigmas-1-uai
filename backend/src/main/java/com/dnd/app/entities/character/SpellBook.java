package com.dnd.app.entities.character;

import com.dnd.app.entities.configuration.Spell;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class SpellBook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "characterId")
    private Character character;

    @ManyToMany
    @JoinTable(name = "spellBookSpells", joinColumns = {
            @JoinColumn(name = "spellBookId")
    }, inverseJoinColumns = @JoinColumn(name = "spellId"))
    @Cascade(MERGE)
    private List<Spell> spells;

    public SpellBook() {
        this.spells = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public List<Spell> getSpells() {
        return spells;
    }

    public void setSpells(List<Spell> spells) {
        this.spells = spells;
    }
}
