package com.dnd.app.entities.character;

import com.dnd.app.dto.character.CharacterHealthAndArmorDTO;

import javax.persistence.*;
import java.util.List;

@Entity
public class CharacterHealthAndArmor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(mappedBy = "characterHealthAndArmor")
    private Character character;

    @Column
    private Long hpMax;

    @Column
    private Long hpCurrent;

    @Column
    private Long armorClass;

    @ElementCollection
    private List<String> defenses;

    @ElementCollection
    private List<String> conditions;

    @Column
    private String damageReduction;

    @Column
    private Long successDeathSaves;

    @Column
    private Long failedDeathSaves;

    public CharacterHealthAndArmor() {
    }

    public CharacterHealthAndArmor(Character character, CharacterHealthAndArmorDTO characterHealthAndArmorDTO) {
        this.character = character;
        this.hpMax = characterHealthAndArmorDTO.getHpMax();
        this.hpCurrent = characterHealthAndArmorDTO.getHpCurrent();
        this.armorClass = characterHealthAndArmorDTO.getArmorClass();
        this.defenses = characterHealthAndArmorDTO.getDefenses();
        this.conditions = characterHealthAndArmorDTO.getConditions();
        this.damageReduction = characterHealthAndArmorDTO.getDamageReduction();
        this.successDeathSaves = characterHealthAndArmorDTO.getSuccessDeathSaves();
        this.failedDeathSaves = characterHealthAndArmorDTO.getFailedDeathSaves();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public Long getHpMax() {
        return hpMax;
    }

    public void setHpMax(Long hpMax) {
        this.hpMax = hpMax;
    }

    public Long getHpCurrent() {
        return hpCurrent;
    }

    public void setHpCurrent(Long hpCurrent) {
        this.hpCurrent = hpCurrent;
    }

    public Long getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(Long armorClass) {
        this.armorClass = armorClass;
    }

    public List<String> getDefenses() {
        return defenses;
    }

    public void setDefenses(List<String> defenses) {
        this.defenses = defenses;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public String getDamageReduction() {
        return damageReduction;
    }

    public void setDamageReduction(String damageReduction) {
        this.damageReduction = damageReduction;
    }

    public Long getSuccessDeathSaves() {
        return successDeathSaves;
    }

    public void setSuccessDeathSaves(Long successDeathSaves) {
        this.successDeathSaves = successDeathSaves;
    }

    public Long getFailedDeathSaves() {
        return failedDeathSaves;
    }

    public void setFailedDeathSaves(Long failedDeathSaves) {
        this.failedDeathSaves = failedDeathSaves;
    }
}
