package com.dnd.app.entities.configuration;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

import static org.hibernate.annotations.CascadeType.ALL;
import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class Race {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToMany
    @JoinTable(name = "raceTraits", joinColumns = {
            @JoinColumn(name = "raceId")
    }, inverseJoinColumns = @JoinColumn(name = "traitId"))
    @Cascade(ALL)
    private Set<Trait> traits;

    @Column
    private Long speed;

    @ManyToMany
    @JoinTable(name = "raceLanguages", joinColumns = {
            @JoinColumn(name = "raceId")
    }, inverseJoinColumns = @JoinColumn(name = "languageId"))
    @Cascade(MERGE)
    private Set<Language> languages;

    @OneToMany
    @JoinColumn(name = "race")
    @Cascade(ALL)
    private Set<ScoreIncrease> scoreIncreases;

    public Race() {
    }

    public Race(String name, Long speed, Set<Language> languages, Set<Trait> traits) {
        this.name = name;
        this.speed = speed;
        this.languages = languages;
        this.traits = traits;
    }

    public void setScoreIncreases(Set<ScoreIncrease> scoreIncreases) {
        this.scoreIncreases = scoreIncreases;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Trait> getTraits() {
        return traits;
    }

    public Long getSpeed() {
        return speed;
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public Set<ScoreIncrease> getScoreIncreases() {
        return scoreIncreases;
    }
}
