package com.dnd.app.entities.configuration;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class ProEfficiency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @OneToOne
    @Cascade(MERGE)
    private EquipmentProEfficiencyType type;

    public ProEfficiency() {
    }

    public ProEfficiency(String name, EquipmentProEfficiencyType type) {
        this.name = name;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EquipmentProEfficiencyType getType() {
        return type;
    }

    public void setType(EquipmentProEfficiencyType type) {
        this.type = type;
    }
}
