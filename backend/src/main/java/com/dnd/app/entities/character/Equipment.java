package com.dnd.app.entities.character;

import javax.persistence.*;

@Entity
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(targetEntity = Character.class)
    @JoinColumn(name = "character")
    private Character character;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private Long ac;

    public Long getId() {
        return id;
    }

    public Character getCharacter() {
        return character;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Long getAc() {
        return ac;
    }

    public Equipment setId(Long id) {
        this.id = id;
        return this;
    }

    public Equipment setCharacter(Character character) {
        this.character = character;
        return this;
    }

    public Equipment setName(String name) {
        this.name = name;
        return this;
    }

    public Equipment setDescription(String description) {
        this.description = description;
        return this;
    }

    public Equipment setAc(Long ac) {
        this.ac = ac;
        return this;
    }
}
