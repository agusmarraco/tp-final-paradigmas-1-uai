package com.dnd.app.entities.configuration;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;

import static org.hibernate.annotations.CascadeType.ALL;
import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class ClassTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    @Cascade(ALL)
    private List<SpellSlotPerClass> spellSlotPerClass;

    @ManyToMany
    @Cascade(ALL)
    @JoinTable(name = "classTemplateSavingThrows", joinColumns = {
    }, inverseJoinColumns = @JoinColumn(name = "savingThrowId"))
    private List<SavingThrow> savingThrows;

    @ManyToMany
    @Cascade(ALL)
    @JoinTable(name = "classTemplateProEfficiencies", joinColumns = {
    }, inverseJoinColumns = @JoinColumn(name = "proEfficiencyId"))
    private List<ProEfficiency> proEfficiencies;

    @ManyToMany
    @Cascade(MERGE)
    @JoinTable(name = "classTemplateSkills", joinColumns = {
    }, inverseJoinColumns = @JoinColumn(name = "skillId"))
    private List<Skill> skills;

    @ManyToMany
    @Cascade(ALL)
    @JoinTable(name = "classTemplateTraits", joinColumns = {
            @JoinColumn(name = "classId")
    }, inverseJoinColumns = @JoinColumn(name = "traitId"))
    private List<Trait> traits;

    @Column
    private String name;

    @OneToOne
    private Ability spellCastingAbility;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SpellSlotPerClass> getSpellSlotPerClass() {
        return spellSlotPerClass;
    }

    public void setSpellSlotPerClass(List<SpellSlotPerClass> spellSlotPerClass) {
        this.spellSlotPerClass = spellSlotPerClass;
    }

    public List<SavingThrow> getSavingThrows() {
        return savingThrows;
    }

    public void setSavingThrows(List<SavingThrow> savingThrows) {
        this.savingThrows = savingThrows;
    }

    public List<Trait> getTraits() {
        return traits;
    }

    public void setTraits(List<Trait> traits) {
        this.traits = traits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ability getSpellCastingAbility() {
        return spellCastingAbility;
    }

    public void setSpellCastingAbility(Ability spellCastingAbility) {
        this.spellCastingAbility = spellCastingAbility;
    }

    public List<ProEfficiency> getProEfficiencies() {
        return proEfficiencies;
    }

    public void setProEfficiencies(List<ProEfficiency> proEfficiencies) {
        this.proEfficiencies = proEfficiencies;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}

