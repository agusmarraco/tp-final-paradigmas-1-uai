package com.dnd.app.entities.configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class ScoreIncrease {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JsonIgnore
    private Race race;

    @OneToOne
    @Cascade(MERGE)
    private Ability ability;

    @Column
    private Long amount;

    public ScoreIncrease() {

    }

    public ScoreIncrease(Race race, Ability ability, Long amount) {
        this.race = race;
        this.ability = ability;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public Race getRace() {
        return race;
    }

    public Ability getAbility() {
        return ability;
    }

    public Long getAmount() {
        return amount;
    }
}
