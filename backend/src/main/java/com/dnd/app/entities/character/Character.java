package com.dnd.app.entities.character;

import com.dnd.app.entities.configuration.ClassTemplate;
import com.dnd.app.entities.configuration.ProEfficiency;
import com.dnd.app.entities.configuration.Race;
import com.dnd.app.entities.user.User;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.annotations.CascadeType.ALL;
import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToOne
    @Cascade(MERGE)
    private ClassTemplate classTemplate;

    @Column
    private String background;

    @Column(nullable = false)
    private Long level;


    @ManyToOne(targetEntity = Race.class)
    @JoinColumn(name = "race")
    @Cascade(MERGE)
    private Race race;

    @Column()
    private String description;

    @Column(nullable = false)
    private Long proEfficiencyBonus;

    @OneToOne(orphanRemoval = true)
    @Cascade(ALL)
    private CharacterHealthAndArmor characterHealthAndArmor;

    @OneToMany(orphanRemoval = true)
    @Cascade(ALL)
    private List<Equipment> equipment;

    @OneToMany
    @Cascade(ALL)
    private List<CharacterSkill> characterSkills;

    @ManyToMany
    @JoinTable(name = "characterProEfficiency", joinColumns = {
            @JoinColumn(name = "characterId")
    }, inverseJoinColumns = @JoinColumn(name = "proEfficiencyId"))
    @Cascade(MERGE)
    private List<ProEfficiency> proEfficiencies;

    @OneToMany
    @Cascade(ALL)
    private List<CharacterAbility> characterAbilities;

    @Column(nullable = false)
    private Boolean hasInspiration;

    @ManyToOne
    @JoinColumn(name = "user")
    private User user;

    @OneToOne
    @Cascade(MERGE)
    private SpellBook spellBook;

    public Character() {
        this.characterAbilities = new ArrayList<>();
        this.proEfficiencies = new ArrayList<>();
        this.characterSkills = new ArrayList<>();
        this.equipment = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ClassTemplate getClassTemplate() {
        return classTemplate;
    }

    public String getBackground() {
        return background;
    }

    public Long getLevel() {
        return level;
    }

    public Race getRace() {
        return race;
    }

    public String getDescription() {
        return description;
    }

    public Long getProEfficiencyBonus() {
        return proEfficiencyBonus;
    }

    public CharacterHealthAndArmor getCharacterHealthAndArmor() {
        return characterHealthAndArmor;
    }

    public List<Equipment> getEquipment() {
        return equipment;
    }

    public List<CharacterSkill> getCharacterSkills() {
        return characterSkills;
    }

    public List<ProEfficiency> getProEfficiencies() {
        return proEfficiencies;
    }

    public List<CharacterAbility> getCharacterAbilities() {
        return characterAbilities;
    }

    public Boolean getHasInspiration() {
        return hasInspiration;
    }

    public SpellBook getSpellBook() {
        return spellBook;
    }

    public Character setId(Long id) {
        this.id = id;
        return this;
    }

    public Character setName(String name) {
        this.name = name;
        return this;
    }

    public Character setClassTemplate(ClassTemplate classTemplate) {
        this.classTemplate = classTemplate;
        return this;
    }

    public Character setBackground(String background) {
        this.background = background;
        return this;
    }

    public Character setLevel(Long level) {
        this.level = level;
        return this;
    }

    public Character setRace(Race race) {
        this.race = race;
        return this;
    }

    public Character setDescription(String description) {
        this.description = description;
        return this;
    }

    public Character setProEfficiencyBonus(Long proEfficiencyBonus) {
        this.proEfficiencyBonus = proEfficiencyBonus;
        return this;
    }

    public Character setCharacterHealthAndArmor(CharacterHealthAndArmor characterHealthAndArmor) {
        this.characterHealthAndArmor = characterHealthAndArmor;
        return this;
    }

    public Character setEquipment(List<Equipment> equipment) {
        this.equipment = equipment;
        return this;
    }

    public Character setCharacterSkills(List<CharacterSkill> characterSkills) {
        this.characterSkills = characterSkills;
        return this;
    }

    public Character setProEfficiencies(List<ProEfficiency> proEfficiencies) {
        this.proEfficiencies = proEfficiencies;
        return this;
    }

    public Character setCharacterAbilities(List<CharacterAbility> characterAbilities) {
        this.characterAbilities = characterAbilities;
        return this;
    }

    public Character setHasInspiration(Boolean hasInspiration) {
        this.hasInspiration = hasInspiration;
        return this;
    }

    public Character setSpellBook(SpellBook spellBook) {
        this.spellBook = spellBook;
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
