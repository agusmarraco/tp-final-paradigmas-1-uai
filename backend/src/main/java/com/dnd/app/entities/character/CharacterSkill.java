package com.dnd.app.entities.character;

import com.dnd.app.entities.configuration.Skill;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class CharacterSkill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = Character.class)
    @JoinColumn(name = "character")
    private Character character;

    @ManyToOne(targetEntity = Skill.class)
    @JoinColumn(name = "skill")
    @Cascade(MERGE)
    private Skill skill;

    @Column
    private Boolean isProEfficient;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    public Boolean getProEfficient() {
        return isProEfficient;
    }

    public void setProEfficient(Boolean proEfficient) {
        isProEfficient = proEfficient;
    }
}
