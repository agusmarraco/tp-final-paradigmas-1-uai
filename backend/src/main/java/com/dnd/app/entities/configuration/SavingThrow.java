package com.dnd.app.entities.configuration;

import javax.persistence.*;

@Entity
public class SavingThrow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Ability ability;

    public SavingThrow() {
    }

    public SavingThrow(Ability ability) {
        this.ability = ability;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }
}
