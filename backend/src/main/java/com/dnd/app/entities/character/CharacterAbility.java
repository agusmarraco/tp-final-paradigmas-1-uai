package com.dnd.app.entities.character;

import com.dnd.app.entities.configuration.Ability;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

import static org.hibernate.annotations.CascadeType.MERGE;

@Entity
public class CharacterAbility {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long baseValue;

    @Column
    private Long externalModifiers;

    @Column
    private Long modifier;

    @ManyToOne
    @JoinColumn(name = "character")
    private Character character;

    @OneToOne
    @Cascade(MERGE)
    private Ability ability;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(Long baseValue) {
        this.baseValue = baseValue;
    }

    public Long getExternalModifiers() {
        return externalModifiers;
    }

    public void setExternalModifiers(Long externalModifiers) {
        this.externalModifiers = externalModifiers;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public Character getCharacter() {
        return character;
    }

    public void setCharacter(Character character) {
        this.character = character;
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }
}
