package com.dnd.app.repositories.character;

import com.dnd.app.entities.configuration.ProEfficiency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProEfficiencyRepository extends JpaRepository<ProEfficiency, Long> {
}
