package com.dnd.app.repositories.character;

import com.dnd.app.entities.character.CharacterAbility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterAbilityRepository extends JpaRepository<CharacterAbility, Long> {
}
