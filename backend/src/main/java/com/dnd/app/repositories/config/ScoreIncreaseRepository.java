package com.dnd.app.repositories.config;

import com.dnd.app.entities.configuration.ScoreIncrease;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScoreIncreaseRepository extends JpaRepository<ScoreIncrease, Long> {
}
