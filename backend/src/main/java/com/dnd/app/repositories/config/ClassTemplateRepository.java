package com.dnd.app.repositories.config;

import com.dnd.app.entities.configuration.ClassTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassTemplateRepository extends JpaRepository<ClassTemplate, Long> {
}
