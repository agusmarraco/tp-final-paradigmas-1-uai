package com.dnd.app.repositories.config;

import com.dnd.app.entities.configuration.SavingThrow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SavingThrowRepository extends JpaRepository<SavingThrow, Long> {
}
