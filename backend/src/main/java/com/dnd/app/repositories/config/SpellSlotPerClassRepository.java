package com.dnd.app.repositories.config;

import com.dnd.app.entities.configuration.SpellSlotPerClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpellSlotPerClassRepository extends JpaRepository<SpellSlotPerClass, Long> {
}
