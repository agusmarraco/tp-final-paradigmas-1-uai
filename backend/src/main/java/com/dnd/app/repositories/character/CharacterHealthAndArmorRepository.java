package com.dnd.app.repositories.character;

import com.dnd.app.entities.character.CharacterHealthAndArmor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterHealthAndArmorRepository extends JpaRepository<CharacterHealthAndArmor, Long> {
}
