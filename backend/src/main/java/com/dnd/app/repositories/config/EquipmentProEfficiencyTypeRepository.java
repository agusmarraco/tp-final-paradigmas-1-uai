package com.dnd.app.repositories.config;

import com.dnd.app.entities.configuration.EquipmentProEfficiencyType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipmentProEfficiencyTypeRepository extends JpaRepository<EquipmentProEfficiencyType, Long> {
}
