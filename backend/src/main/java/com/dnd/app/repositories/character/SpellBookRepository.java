package com.dnd.app.repositories.character;

import com.dnd.app.entities.character.SpellBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpellBookRepository extends JpaRepository<SpellBook, Long> {

    Optional<SpellBook> findByCharacterId(Long characterId);

    void deleteByCharacterId(Long characterId);
}
