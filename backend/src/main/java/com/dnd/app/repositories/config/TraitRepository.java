package com.dnd.app.repositories.config;

import com.dnd.app.entities.configuration.Trait;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TraitRepository extends JpaRepository<Trait, Long> {
}
