package com.dnd.app.repositories.character;

import com.dnd.app.entities.character.CharacterSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterSkillRepository extends JpaRepository<CharacterSkill, Long> {
}
