package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.Spell;

public class SpellDTO {
    private Long id;
    private String name;
    private String description;
    private Long level;
    private String range;
    private String duration;
    private Boolean concentration;
    private String castingTime;

    public SpellDTO() {

    }

    public SpellDTO(Spell spell) {
        this.id = spell.getId();
        this.name = spell.getName();
        this.description = spell.getDescription();
        this.level = spell.getLevel();
        this.range = spell.getRange();
        this.duration = spell.getDuration();
        this.concentration = spell.getConcentration();
        this.castingTime = spell.getCastingTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Boolean getConcentration() {
        return concentration;
    }

    public void setConcentration(Boolean concentration) {
        this.concentration = concentration;
    }

    public String getCastingTime() {
        return castingTime;
    }

    public void setCastingTime(String castingTime) {
        this.castingTime = castingTime;
    }
}
