package com.dnd.app.dto.character;

import com.dnd.app.dto.configuration.SpellDTO;
import com.dnd.app.entities.character.SpellBook;

import java.util.List;
import java.util.stream.Collectors;

public class SpellBookDTO {

    private Long id;
    private Long characterId;
    private List<SpellDTO> spells;

    public SpellBookDTO() {
    }

    public SpellBookDTO(SpellBook spellBook) {
        this.id = spellBook.getId();
        this.characterId = spellBook.getCharacter().getId();
        this.spells = spellBook.getSpells().stream().map(x -> new SpellDTO(x)).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public List<SpellDTO> getSpells() {
        return spells;
    }

    public void setSpells(List<SpellDTO> spells) {
        this.spells = spells;
    }
}
