package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.Trait;

public class TraitDTO {

    private Long id;
    private String name;
    private String description;


    public TraitDTO() {
    }

    public TraitDTO(Trait trait) {
        this.id = trait.getId();
        this.name = trait.getName();
        this.description = trait.getDescription();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
