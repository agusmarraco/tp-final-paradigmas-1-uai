package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.ScoreIncrease;

public class ScoreIncreaseDTO {
    private Long id;
    private AbilityDTO ability;
    private Long amount;

    public ScoreIncreaseDTO() {
    }

    public ScoreIncreaseDTO(ScoreIncrease scoreIncrease) {
        this.id = scoreIncrease.getId();
        this.ability = new AbilityDTO(scoreIncrease.getAbility());
        this.amount = scoreIncrease.getAmount();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AbilityDTO getAbility() {
        return ability;
    }

    public void setAbility(AbilityDTO ability) {
        this.ability = ability;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
