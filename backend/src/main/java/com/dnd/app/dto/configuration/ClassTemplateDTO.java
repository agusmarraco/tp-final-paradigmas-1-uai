package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.ClassTemplate;

import java.util.List;
import java.util.stream.Collectors;

public class ClassTemplateDTO {
    private Long id;
    private List<SpellSlotPerClassDTO> spellSlotPerClass;
    private List<SavingThrowDTO> savingThrows;
    private List<ProEfficiencyDTO> proEfficiencies;
    private List<SkillDTO> skills;
    private List<TraitDTO> traits;
    private String name;
    private AbilityDTO spellCastingAbility;

    public ClassTemplateDTO() {

    }

    public ClassTemplateDTO(ClassTemplate classTemplate) {
        this.id = classTemplate.getId();
        this.name = classTemplate.getName();
        this.spellCastingAbility = classTemplate.getSpellCastingAbility() != null ? new AbilityDTO(classTemplate.getSpellCastingAbility()) : null;
        this.spellSlotPerClass = classTemplate.getSpellSlotPerClass().stream().map(x -> new SpellSlotPerClassDTO(x)).collect(Collectors.toList());
        this.savingThrows = classTemplate.getSavingThrows().stream().map(x -> new SavingThrowDTO(x)).collect(Collectors.toList());
        this.proEfficiencies = classTemplate.getProEfficiencies().stream().map(x -> new ProEfficiencyDTO(x)).collect(Collectors.toList());
        this.skills = classTemplate.getSkills().stream().map(x -> new SkillDTO(x)).collect(Collectors.toList());
        this.traits = classTemplate.getTraits().stream().map(x -> new TraitDTO(x)).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SpellSlotPerClassDTO> getSpellSlotPerClass() {
        return spellSlotPerClass;
    }

    public void setSpellSlotPerClass(List<SpellSlotPerClassDTO> spellSlotPerClass) {
        this.spellSlotPerClass = spellSlotPerClass;
    }

    public List<SavingThrowDTO> getSavingThrows() {
        return savingThrows;
    }

    public void setSavingThrows(List<SavingThrowDTO> savingThrows) {
        this.savingThrows = savingThrows;
    }

    public List<ProEfficiencyDTO> getProEfficiencies() {
        return proEfficiencies;
    }

    public void setProEfficiencies(List<ProEfficiencyDTO> proEfficiencies) {
        this.proEfficiencies = proEfficiencies;
    }

    public List<SkillDTO> getSkills() {
        return skills;
    }

    public void setSkills(List<SkillDTO> skills) {
        this.skills = skills;
    }

    public List<TraitDTO> getTraits() {
        return traits;
    }

    public void setTraits(List<TraitDTO> traits) {
        this.traits = traits;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AbilityDTO getSpellCastingAbility() {
        return spellCastingAbility;
    }

    public void setSpellCastingAbility(AbilityDTO spellCastingAbility) {
        this.spellCastingAbility = spellCastingAbility;
    }
}
