package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.EquipmentProEfficiencyType;

public class EquipmentProEfficiencyTypeDTO {
    private Long id;
    private String type;

    public EquipmentProEfficiencyTypeDTO() {
    }

    public EquipmentProEfficiencyTypeDTO(EquipmentProEfficiencyType type) {
        this.id = type.getId();
        this.type = type.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
