package com.dnd.app.dto.character;

import com.dnd.app.entities.character.Equipment;

public class EquipmentDTO {
    private Long id;
    private String name;
    private String description;
    private Long ac;

    public EquipmentDTO() {

    }

    public EquipmentDTO(Equipment equipment) {
        this.id = equipment.getId();
        this.name = equipment.getName();
        this.description = equipment.getDescription();
        this.ac = equipment.getAc();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAc() {
        return ac;
    }

    public void setAc(Long ac) {
        this.ac = ac;
    }
}
