package com.dnd.app.dto.character;

import com.dnd.app.dto.configuration.ProEfficiencyDTO;
import com.dnd.app.entities.character.Character;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

public class CharacterDTO {

    private Long id;
    @NotEmpty
    private String name;
    @NotNull
    private Long classId;
    @NotEmpty
    private String background;
    @NotNull
    private Long level;
    @NotNull
    private Long raceId;
    @NotEmpty
    private String description;
    @NotNull
    private Long proEfficiencyBonus;
    private CharacterHealthAndArmorDTO characterHealthAndArmor;
    private List<EquipmentDTO> equipment;
    private List<CharacterSkillDTO> characterSkills;
    private List<ProEfficiencyDTO> proEfficiencies;
    private List<CharacterAbilityDTO> characterAbilities;
    @NotNull
    private Boolean hasInspiration;

    public CharacterDTO() {

    }

    public CharacterDTO(Character character) {
        this.id = character.getId();
        this.name = character.getName();
        this.classId = character.getClassTemplate().getId();
        this.background = character.getBackground();
        this.level = character.getLevel();
        this.raceId = character.getRace().getId();
        this.description = character.getDescription();
        this.proEfficiencyBonus = character.getProEfficiencyBonus();
        this.characterHealthAndArmor = new CharacterHealthAndArmorDTO(character.getCharacterHealthAndArmor());
        this.equipment = character.getEquipment().stream().map(x -> new EquipmentDTO(x)).collect(Collectors.toList());
        this.characterSkills = character.getCharacterSkills().stream().map(x -> new CharacterSkillDTO(x)).collect(Collectors.toList());
        this.proEfficiencies = character.getProEfficiencies().stream().map(x -> new ProEfficiencyDTO(x)).collect(Collectors.toList());
        this.characterAbilities = character.getCharacterAbilities().stream().map(x -> new CharacterAbilityDTO(x)).collect(Collectors.toList());
        this.hasInspiration = character.getHasInspiration();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getClassId() {
        return classId;
    }

    public void setClassId(Long classId) {
        this.classId = classId;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getRaceId() {
        return raceId;
    }

    public void setRaceId(Long raceId) {
        this.raceId = raceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProEfficiencyBonus() {
        return proEfficiencyBonus;
    }

    public void setProEfficiencyBonus(Long proEfficiencyBonus) {
        this.proEfficiencyBonus = proEfficiencyBonus;
    }

    public CharacterHealthAndArmorDTO getCharacterHealthAndArmor() {
        return characterHealthAndArmor;
    }

    public void setCharacterHealthAndArmor(CharacterHealthAndArmorDTO characterHealthAndArmor) {
        this.characterHealthAndArmor = characterHealthAndArmor;
    }

    public List<EquipmentDTO> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<EquipmentDTO> equipment) {
        this.equipment = equipment;
    }

    public List<CharacterSkillDTO> getCharacterSkills() {
        return characterSkills;
    }

    public void setCharacterSkills(List<CharacterSkillDTO> characterSkills) {
        this.characterSkills = characterSkills;
    }

    public List<ProEfficiencyDTO> getProEfficiencies() {
        return proEfficiencies;
    }

    public void setProEfficiencies(List<ProEfficiencyDTO> proEfficiencies) {
        this.proEfficiencies = proEfficiencies;
    }

    public List<CharacterAbilityDTO> getCharacterAbilities() {
        return characterAbilities;
    }

    public void setCharacterAbilities(List<CharacterAbilityDTO> characterAbilities) {
        this.characterAbilities = characterAbilities;
    }

    public Boolean getHasInspiration() {
        return hasInspiration;
    }

    public void setHasInspiration(Boolean hasInspiration) {
        this.hasInspiration = hasInspiration;
    }
}
