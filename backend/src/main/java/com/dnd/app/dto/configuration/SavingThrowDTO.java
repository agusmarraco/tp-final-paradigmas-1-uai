package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.SavingThrow;

public class SavingThrowDTO {
    private Long id;
    private AbilityDTO ability;

    public SavingThrowDTO() {
    }

    public SavingThrowDTO(SavingThrow savingThrow) {
        this.id = savingThrow.getId();
        this.ability = new AbilityDTO(savingThrow.getAbility());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AbilityDTO getAbility() {
        return ability;
    }

    public void setAbility(AbilityDTO ability) {
        this.ability = ability;
    }
}
