package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.SpellSlotPerClass;

public class SpellSlotPerClassDTO {

    private Long id;
    private Long level;
    private Long level0Slot;
    private Long level1Slot;
    private Long level2Slot;
    private Long level3Slot;
    private Long level4Slot;
    private Long level5Slot;
    private Long level6Slot;
    private Long level7Slot;
    private Long level8Slot;
    private Long level9Slot;

    public SpellSlotPerClassDTO() {
    }

    public SpellSlotPerClassDTO(SpellSlotPerClass spellSlotPerClass) {
        this.id = spellSlotPerClass.getId();
        this.level = spellSlotPerClass.getLevel();
        this.level0Slot = spellSlotPerClass.getLevel0Slot();
        this.level1Slot = spellSlotPerClass.getLevel1Slot();
        this.level2Slot = spellSlotPerClass.getLevel2Slot();
        this.level3Slot = spellSlotPerClass.getLevel3Slot();
        this.level4Slot = spellSlotPerClass.getLevel4Slot();
        this.level5Slot = spellSlotPerClass.getLevel5Slot();
        this.level6Slot = spellSlotPerClass.getLevel6Slot();
        this.level7Slot = spellSlotPerClass.getLevel7Slot();
        this.level8Slot = spellSlotPerClass.getLevel8Slot();
        this.level9Slot = spellSlotPerClass.getLevel9Slot();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getLevel0Slot() {
        return level0Slot;
    }

    public void setLevel0Slot(Long level0Slot) {
        this.level0Slot = level0Slot;
    }

    public Long getLevel1Slot() {
        return level1Slot;
    }

    public void setLevel1Slot(Long level1Slot) {
        this.level1Slot = level1Slot;
    }

    public Long getLevel2Slot() {
        return level2Slot;
    }

    public void setLevel2Slot(Long level2Slot) {
        this.level2Slot = level2Slot;
    }

    public Long getLevel3Slot() {
        return level3Slot;
    }

    public void setLevel3Slot(Long level3Slot) {
        this.level3Slot = level3Slot;
    }

    public Long getLevel4Slot() {
        return level4Slot;
    }

    public void setLevel4Slot(Long level4Slot) {
        this.level4Slot = level4Slot;
    }

    public Long getLevel5Slot() {
        return level5Slot;
    }

    public void setLevel5Slot(Long level5Slot) {
        this.level5Slot = level5Slot;
    }

    public Long getLevel6Slot() {
        return level6Slot;
    }

    public void setLevel6Slot(Long level6Slot) {
        this.level6Slot = level6Slot;
    }

    public Long getLevel7Slot() {
        return level7Slot;
    }

    public void setLevel7Slot(Long level7Slot) {
        this.level7Slot = level7Slot;
    }

    public Long getLevel8Slot() {
        return level8Slot;
    }

    public void setLevel8Slot(Long level8Slot) {
        this.level8Slot = level8Slot;
    }

    public Long getLevel9Slot() {
        return level9Slot;
    }

    public void setLevel9Slot(Long level9Slot) {
        this.level9Slot = level9Slot;
    }
}
