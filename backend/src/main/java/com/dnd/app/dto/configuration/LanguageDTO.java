package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.Language;

public class LanguageDTO {
    private Long id;
    private String name;

    public LanguageDTO() {
    }

    public LanguageDTO(Language language) {
        this.id = language.getId();
        this.name = language.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
