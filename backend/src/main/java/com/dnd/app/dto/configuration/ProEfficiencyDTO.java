package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.ProEfficiency;

public class ProEfficiencyDTO {
    private Long id;
    private String name;
    private EquipmentProEfficiencyTypeDTO type;


    public ProEfficiencyDTO() {
    }

    public ProEfficiencyDTO(ProEfficiency proEfficiency) {
        this.id = proEfficiency.getId();
        this.name = proEfficiency.getName();
        this.type = new EquipmentProEfficiencyTypeDTO(proEfficiency.getType());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EquipmentProEfficiencyTypeDTO getType() {
        return type;
    }

    public void setType(EquipmentProEfficiencyTypeDTO type) {
        this.type = type;
    }
}
