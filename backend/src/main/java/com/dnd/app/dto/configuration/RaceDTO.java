package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.Race;

import java.util.Set;
import java.util.stream.Collectors;

public class RaceDTO {
    private Long id;
    private String name;
    private Set<TraitDTO> traits;
    private Long speed;
    private Set<LanguageDTO> languages;
    private Set<ScoreIncreaseDTO> scoreIncreases;

    public RaceDTO() {
    }

    public RaceDTO(Race race) {
        this.id = race.getId();
        this.name = race.getName();
        this.speed = race.getSpeed();
        this.traits = race.getTraits().stream().map(x -> new TraitDTO(x)).collect(Collectors.toSet());
        this.languages = race.getLanguages().stream().map(x -> new LanguageDTO(x)).collect(Collectors.toSet());
        this.scoreIncreases = race.getScoreIncreases().stream().map(x -> new ScoreIncreaseDTO(x)).collect(Collectors.toSet());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TraitDTO> getTraits() {
        return traits;
    }

    public void setTraits(Set<TraitDTO> traits) {
        this.traits = traits;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public Set<LanguageDTO> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<LanguageDTO> languages) {
        this.languages = languages;
    }

    public Set<ScoreIncreaseDTO> getScoreIncreases() {
        return scoreIncreases;
    }

    public void setScoreIncreases(Set<ScoreIncreaseDTO> scoreIncreases) {
        this.scoreIncreases = scoreIncreases;
    }
}
