package com.dnd.app.dto.character;

import com.dnd.app.entities.character.CharacterHealthAndArmor;

import java.util.List;

public class CharacterHealthAndArmorDTO {

    private Long id;
    private Long hpMax;
    private Long hpCurrent;
    private Long armorClass;
    private List<String> defenses;
    private List<String> conditions;
    private String damageReduction;
    private Long successDeathSaves;
    private Long failedDeathSaves;

    public CharacterHealthAndArmorDTO() {

    }

    public CharacterHealthAndArmorDTO(CharacterHealthAndArmor characterHealthAndArmor) {
        this.id = characterHealthAndArmor.getId();
        this.hpMax = characterHealthAndArmor.getHpMax();
        this.hpCurrent = characterHealthAndArmor.getHpCurrent();
        this.armorClass = characterHealthAndArmor.getArmorClass();
        this.defenses = characterHealthAndArmor.getDefenses();
        this.conditions = characterHealthAndArmor.getConditions();
        this.damageReduction = characterHealthAndArmor.getDamageReduction();
        this.successDeathSaves = characterHealthAndArmor.getSuccessDeathSaves();
        this.failedDeathSaves = characterHealthAndArmor.getFailedDeathSaves();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHpMax() {
        return hpMax;
    }

    public void setHpMax(Long hpMax) {
        this.hpMax = hpMax;
    }

    public Long getHpCurrent() {
        return hpCurrent;
    }

    public void setHpCurrent(Long hpCurrent) {
        this.hpCurrent = hpCurrent;
    }

    public Long getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(Long armorClass) {
        this.armorClass = armorClass;
    }

    public List<String> getDefenses() {
        return defenses;
    }

    public void setDefenses(List<String> defenses) {
        this.defenses = defenses;
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public String getDamageReduction() {
        return damageReduction;
    }

    public void setDamageReduction(String damageReduction) {
        this.damageReduction = damageReduction;
    }

    public Long getSuccessDeathSaves() {
        return successDeathSaves;
    }

    public void setSuccessDeathSaves(Long successDeathSaves) {
        this.successDeathSaves = successDeathSaves;
    }

    public Long getFailedDeathSaves() {
        return failedDeathSaves;
    }

    public void setFailedDeathSaves(Long failedDeathSaves) {
        this.failedDeathSaves = failedDeathSaves;
    }
}
