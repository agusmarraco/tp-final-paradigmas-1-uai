package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.Ability;

public class AbilityDTO {
    private Long id;
    private String code;
    private String label;

    public AbilityDTO() {
    }

    public AbilityDTO(Ability ability) {
        this.label = ability.getLabel();
        this.code = ability.getCode();
        this.id = ability.getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
