package com.dnd.app.dto.character;

import com.dnd.app.dto.configuration.SkillDTO;
import com.dnd.app.entities.character.CharacterSkill;

public class CharacterSkillDTO {


    private Long id;

    private SkillDTO skill;

    private Boolean proEfficient;

    public CharacterSkillDTO() {

    }

    public CharacterSkillDTO(CharacterSkill characterSkill) {
        this.id = characterSkill.getId();
        this.skill = new SkillDTO(characterSkill.getSkill());
        this.proEfficient = characterSkill.getProEfficient();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SkillDTO getSkill() {
        return skill;
    }

    public void setSkill(SkillDTO skill) {
        this.skill = skill;
    }

    public Boolean getProEfficient() {
        return proEfficient;
    }

    public void setProEfficient(Boolean proEfficient) {
        this.proEfficient = proEfficient;
    }
}
