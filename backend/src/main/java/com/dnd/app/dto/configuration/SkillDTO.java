package com.dnd.app.dto.configuration;

import com.dnd.app.entities.configuration.Skill;

public class SkillDTO {

    private Long id;
    private AbilityDTO ability;
    private String name;

    public SkillDTO() {
    }

    public SkillDTO(Skill skill) {
        this.id = skill.getId();
        this.ability = new AbilityDTO(skill.getAbility());
        this.name = skill.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AbilityDTO getAbility() {
        return ability;
    }

    public void setAbility(AbilityDTO ability) {
        this.ability = ability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
