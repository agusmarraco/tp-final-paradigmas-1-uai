package com.dnd.app.dto.character;

import com.dnd.app.dto.configuration.AbilityDTO;
import com.dnd.app.entities.character.CharacterAbility;

public class CharacterAbilityDTO {

    private Long id;
    private Long baseValue;
    private Long externalModifiers;
    private Long modifier;
    private AbilityDTO ability;

    public CharacterAbilityDTO() {

    }

    public CharacterAbilityDTO(CharacterAbility characterAbility) {
        this.id = characterAbility.getId();
        this.baseValue = characterAbility.getBaseValue();
        this.externalModifiers = characterAbility.getExternalModifiers();
        this.modifier = characterAbility.getModifier();
        this.ability = new AbilityDTO(characterAbility.getAbility());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(Long baseValue) {
        this.baseValue = baseValue;
    }

    public Long getExternalModifiers() {
        return externalModifiers;
    }

    public void setExternalModifiers(Long externalModifiers) {
        this.externalModifiers = externalModifiers;
    }

    public Long getModifier() {
        return modifier;
    }

    public void setModifier(Long modifier) {
        this.modifier = modifier;
    }

    public AbilityDTO getAbility() {
        return ability;
    }

    public void setAbility(AbilityDTO ability) {
        this.ability = ability;
    }
}
