package com.dnd.app;

import com.dnd.app.services.config.DataInitializationService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.sql.SQLException;

@Configuration
@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories("com.dnd.app.repositories")
@EntityScan("com.dnd.app.entities")
public class FinalApplication {

    private static Server server;

    @Autowired
    private DataInitializationService dataInitializationService;

    public static void main(String[] args) {
        initServer();
        SpringApplication.run(FinalApplication.class, args);
    }

    private static void initServer() {
        try {
            System.out.println("SERVER STARTED");
            server = Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9090").start();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @PostConstruct
    public void preloadData() {
        dataInitializationService.initDatabaseData();
    }

    @PreDestroy
    public void onDestroy() throws Exception {
        server.stop();
    }

    @Bean
    public ObjectMapper defaultObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_SELF_REFERENCES_AS_NULL, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
        return objectMapper;
    }


    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

}
