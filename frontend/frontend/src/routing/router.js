import React from "react";
import { Layout } from 'antd';
import Login from "../pages/login/login.js";
import * as ConfigController from '../api/configController'
import CharacterCreationPage from "../pages/characterCreation/characterCreation.js";
const { Content } = Layout;

const routes = [
    {
        path: "/signup",
        component: SignUp,
        exact: true

    },
    {
        path: "/login",
        component: Login,
        exact: true

    },
    {
        path: "/logout",
        component: LogOut,
        exact: true
    }, {
        path: "/",
        component: DefaultBody,
        exact: true
    }, {
        path: "/viewCharacter",
        component: ViewCharacter,
        exact: true
    }, {
        path: "/createCharacter",
        component: CharacterCreationPage,
        exact: true
    }

];

function LogOut() {
    return window.location.reload();
}

function SignUp() {
    ConfigController.getSpells()
    return <h2>asdasdasdasdasda</h2>
}

function ViewCharacter() {
    return <h2>ViewCharacter</h2>
}


function DefaultBody() {
    return (<Content
        className="site-layout-background"
        style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
        }}>Content
    </Content>)
}

export default routes;