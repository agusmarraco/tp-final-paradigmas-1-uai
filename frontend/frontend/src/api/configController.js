import axios from 'axios';
import Cookies from "js-cookie"
const baseUrl = 'http://localhost:8080';
function getSpells() {
    const headers = {
        "Content-Type": "application/json"
    }
    axios.get(baseUrl.concat("/public/config/spells"), {
        headers: headers
    })
    .then((response) => {
        console.log(response);
    }).catch((error) => {
        console.log(error);
    })
}

function postLogin( user, pass) {
    const headers = {
        "Content-Type": "application/json"
    }
    const data = {
        username : user,
        password: pass,
    }
    axios.post(baseUrl.concat("/user/signin"), data, {
        headers: headers,
    })
    .then((response) => {
        console.log(response);
        Cookies.set("Authorization",(response.data.token));
    }).catch((error) => {
        console.log(error);
    })
}



export {getSpells, postLogin}