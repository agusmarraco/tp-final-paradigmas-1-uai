import React from "react";
import "antd/dist/antd.css";
import "./characterCreation.css";
import { Steps, Button } from "antd";
import ClassSelection from "../../components/classSelection/ClassSelection";
import CharacterInfo from "../../components/characterInfo/CharacterInfo";
import RaceSelection from "../../components/raceSelection/RaceSelection";
import { Layout } from "antd";
import StepContent from "../../components/stepContent/StepContent";
const { Content } = Layout;
const { Step } = Steps;

export default class CharacterCreationPage extends React.Component {
  constructor(props) {
    super(props);
    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
    this.done = this.done.bind(this);
    this.raceRef = React.createRef();
    this.classRef = React.createRef();
    this.infoRef = React.createRef();
    this.generateButtons = this.generateButtons.bind(this);
    this.renderContent = this.renderContent.bind(this);
    this.state = {
      current: 0,
    };
  }

  generateButtons() {
    let buttons = [];
    let next = <StepButton text="Next" onClick={this.next} type="primary" />;
    let previous = <StepButton text="Previous" onClick={this.prev} />;
    let done = <StepButton text="Done" onClick={this.done} type="primary" />;
    const { current } = this.state;
    if (current === 0) {
      buttons.push(next);
    } else if (current === 1) {
      buttons.push(previous, next);
    } else {
      buttons.push(previous, done);
    }
    return buttons;
  }
  next() {
    let newCurrent = this.state.current + 1;
    this.setState({ current: newCurrent });
  }

  prev() {
    let newCurrent = this.state.current - 1;
    this.setState({ current: newCurrent });
  }

  renderContent() {
    let content = [];
    const { current } = this.state;
    if (current === 0) {
      content = <CharacterInfo ref={this.infoRef} />;
    } else if (current === 1) {
      content = <RaceSelection ref={this.raceRef}/>;
    } else {
      content = <ClassSelection ref={this.classRef}/>;
    }
    return content;
  }

  done() {
    alert("todo termino");
  }

  render() {
    const current = this.state.current;
    return (
      <Content className="content-style">
        <Steps current={current}>
          {steps.map((item) => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <StepContent
          content={this.renderContent}
          buttons={this.generateButtons}
        />
      </Content>
    );
  }
}

function WOLO(props) {
  return "asdasd" + props.index;
}

function StepButton(props) {
  return (
    <Button
      style={{ margin: "0 8px" }}
      onClick={props.onClick}
      type={props.type}
    >
      {props.text}
    </Button>
  );
}

const steps = [
  {
    title: "Character Information",
  },
  {
    title: "Select your Race",
  },
  {
    title: "Select your Class",
  },
];
