import React from "react";
import "antd/dist/antd.css";
import "./login.css";
import { Form, Input, Button } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import * as ConfigController from "../../api/configController"
import  { Redirect } from 'react-router-dom'

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username : "",
            password: "",
            redirect: false
        }
        this.login = this.login.bind(this)
        this.updateName = this.updateName.bind(this)
        this.updatePass= this.updatePass.bind(this)
    }
  onFinish = (values) => {
    console.log("Received values of form: ", values);
  };

  login() {
    ConfigController.postLogin(this.state.username, this.state.password);
    this.setState({
        redirect: !this.state.redirect
    })
  }

  updateName(event){
    this.setState({
        username:event.target.value
    })
  }

  updatePass(event){
    this.setState({
        password:event.target.value
    })
  }
  render() {
    if(this.state.redirect){
        return <Redirect to="/"></Redirect>     
    }
    return (
      
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={this.onFinish}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: "Please input your Username!",
            },
          ]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Username" value={this.state.username} onChange={this.updateName}
          />
        </Form.Item>
        <Form.Item
          name="password" value={this.state.password} 
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password" onChange={this.updatePass}
          />
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button" onClick={this.login}
          >
            Log in
          </Button>
          Or <a href="/signup">register now!</a>
        </Form.Item>
      </Form>
    );
  }
}
