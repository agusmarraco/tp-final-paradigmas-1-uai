import React from "react";
import "antd/dist/antd.css";
import "../../index.css";
import { Menu } from "antd";
import { OrcIcon, SpellBookIcon, CastleIcon } from "../icon/index.js";
import Cookies from "js-cookie";

import { NavLink } from "react-router-dom";

const { SubMenu } = Menu;

export default class SideMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authorization: props.authCookiePresent,
      authCallback: props.authCallback
    }
    this.logOut = this.logOut.bind(this);
  }
  handleClick = (e) => {};

  logOut() {
    Cookies.remove("Authorization");
    this.setState({
      authorization: !this.state.authorization
    })
    this.state.authCallback()
  }
    render() {
    return (
      <Menu
        onClick={this.handleClick}
        theme="dark"
        defaultSelectedKeys={["1"]}
        defaultOpenKeys={["sub1"]}
        mode="inline"
      >
        <Menu.Item title={<span>Profile</span>} icon={<CastleIcon />}>
          <NavLink to="/">Home</NavLink>
        </Menu.Item>
        <SubMenu
          key="sub1"
          title={
            <span>
              <SpellBookIcon />
              <span>Profile</span>
            </span>
          }
        >
          <Menu.Item key="1" disabled={this.state.authorization}>
            <NavLink to="/signup">Sing Up</NavLink>
          </Menu.Item>
          <Menu.Item key="2" disabled={this.state.authorization}>
            <NavLink to="/login">Log In</NavLink>
          </Menu.Item>
          <Menu.Item key="3" disabled={!this.state.authorization} onClick={this.logOut}>
            Log Out
          </Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" icon={<OrcIcon />} title="Characters">
          <Menu.Item key="5" disabled={!this.state.authorization}>
            <NavLink to="/viewCharacter">View</NavLink>
          </Menu.Item>
          <Menu.Item key="6" disabled={!this.state.authorization}>
            <NavLink to="/createCharacter">Create</NavLink>
          </Menu.Item>
        </SubMenu>
      </Menu>
    );
  }
}
