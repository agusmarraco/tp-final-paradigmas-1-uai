import React from 'react';
import 'antd/dist/antd.css';
import Icon from '@ant-design/icons';
import {ReactComponent as orc} from './orc.svg';
import {ReactComponent as spellbook} from './spell.svg';
import {ReactComponent as castle} from './castle.svg';
import './index.css'


const OrcIcon = props => <Icon className="icon-size" component={orc} {...props} />;

const SpellBookIcon = props => <Icon  className="icon-size" component={spellbook} {...props} />;
const CastleIcon = props => <Icon  className="icon-size" component={castle} {...props} />;

export {OrcIcon, SpellBookIcon, CastleIcon};