import React from 'react';
import 'antd/dist/antd.css';
import '../../index.css';
import { Layout } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';
import {
  BrowserRouter,
  Route
} from "react-router-dom";
import SideMenu from '../menu/Menu';
import routes from '../../routing/router.js'
import Cookies from "js-cookie";
import { render } from '@testing-library/react';

const { Header, Sider } = Layout;
const routeComponents =  
  routes.map(((route, i) => (
    <Route path={route.path} key={i} component={route.component} exact={route.exact}/>)))


class Nav extends React.Component {
  constructor(props){
    super(props)
    this.state ={
      collapsed: false, 
      authCookiePresent: Cookies.get("Authorization") !== undefined
    };
    this.updateAuthCookie = this.updateAuthCookie.bind(this); 
  }


  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  updateAuthCookie(){
      this.setState({
        authCookiePresent: Cookies.get("Authorization") !== undefined
      })
      render();
  }

  render() {
    return (
      <BrowserRouter >
        <Layout style={{ height: "100%" }}>
          <Sider trigger={null} collapsible collapsed={this.state.collapsed} collapsedWidth={0} >
            <SideMenu authCallback={this.updateAuthCookie} authCookiePresent={this.state.authCookiePresent}></SideMenu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background" style={{ padding: 0, float: "left" }}>
              {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: this.toggle,
              })}
            </Header>
            {routeComponents}
          </Layout>
        </Layout>
      </BrowserRouter >

    );
  }
}

export default Nav;