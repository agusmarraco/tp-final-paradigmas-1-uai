import React from "react";
import "./StepContent.css";

export default class StepContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttons: props.buttons,
      content: props.content
        };
  }

  render() {
    return (
      <div>
        <div className="steps-content">{this.state.content()}</div>
        <div className="steps-action">
        
            {
                this.state.buttons()
            }
        </div>
      </div>
    );
  }
}
